import React, {Component} from 'react';
import '../App.css';

class AsteroidCard extends Component{
    render(){
        const props = this.props;
        return(
            <section className="asteroid-card" onClick={() => props.showDetail(props.neo_reference_id)}>
                <label><b>Name :</b> {props.name}</label>
                <label><b>Min size :</b> {props.estimated_diameter_min} km</label>
                <label><b>Max size :</b> {props.estimated_diameter_max} km</label>
                <label><b>Passing date :</b> {props.next_passage}</label>
                <label><b>Passing distance :</b> {props.miss_distance}</label>
            </section>
        )
    }
}

export default AsteroidCard