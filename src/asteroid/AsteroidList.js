import React, {Component} from 'react';
import '../App.css';
import AsteroidDetailList from '../detail/AsteroidDetailList';
import AsteroidCard from "./AsteroidCard";
import * as Constants from "../constants/constants"

class AsteroidList extends Component{

    state = {
        closeApprochData: {},
        neoReferenceId: null,
    }

    async getAsteroidDetailForId(id){
        const params = {
            id:encodeURIComponent(id), 
            api_key:Constants.API_KEY + "=" + encodeURIComponent(Constants.API_KEY_VALUE)
        }
        try {
            fetch(Constants.BASE_URL + `/neo/${params.id}?${params.api_key}`)
            .then(res => res.json())
            .then((data) => {
              this.setState({
                    closeApprochData: data,
              })
          })
        } catch (error) {
            
        }
    }

    handleSelect = neo_reference_id => {
        if(neo_reference_id !== null){
            this.getAsteroidDetailForId(neo_reference_id)
        }
    }

    render(){

        const props = this.props;

        const {isLoading, isSearching, isLoaded, isError, message} = this.props
        
        let asteroidsView; 

        if(isError || isLoading || isSearching){
            asteroidsView = <h3>{message}</h3>;
        }

        if (isLoaded) {
            const sd_asteroids = props.asteroids[props.start_date]
            const ed_asteroids = props.asteroids[props.end_date]
            const combined = sd_asteroids.concat(ed_asteroids)
            asteroidsView = combined.map(asteroid => {
            return(
                <AsteroidCard
                    neo_reference_id={asteroid[Constants.NEO_REFERENCE_ID_KEY]} 
                    name={asteroid[Constants.NAME_KEY]}
                    estimated_diameter_min={asteroid[Constants.ESTIMATED_DIAMETER_KEY][Constants.KILOMETERS_KEY][Constants.ESTIMATED_DIAMETER_MIN_KEY]}
                    estimated_diameter_max={asteroid[Constants.ESTIMATED_DIAMETER_KEY][Constants.KILOMETERS_KEY][Constants.ESTIMATED_DIAMETER_MAX_KEY]}
                    next_passage={asteroid[Constants.CLOSE_APPROACH_DATA_KEY][0][Constants.CLOSE_APPROACH_DATE_KEY]}
                    miss_distance={asteroid[Constants.CLOSE_APPROACH_DATA_KEY][0][Constants.MISS_DISTANCE_KEY][Constants.KILOMETERS_KEY]}
                    showDetail={this.handleSelect}
                ></AsteroidCard>
            )
            })
        }
        return (
        <div className="detail">
            <section className="asteroid-list">
                <h3 className="top">Asteroids</h3>
                {asteroidsView}
            </section>
            <section className="asteroid-detail-list">
                <h3>Asteroid informations</h3>
                <AsteroidDetailList
                    closeApproachData = {this.state.closeApprochData}
                ></AsteroidDetailList>
            </section>
        </div>
        );
    } 
}

export default AsteroidList