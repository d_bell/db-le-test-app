import React from 'react';
import '../App.css';
import * as Constants from "../constants/constants"

class Form extends React.Component {
    state = {
        start_date: '',
        end_date: '',
        asteroids: [],
        isSearching: false
    };
    
    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({[name] : value})
    }
      
    handleSubmit = async (event) => {
        event.preventDefault();
        this.props.onSubmit({isSearching:true})
        this.getAsteroidsTraficBetweenDate()
    };

    async getAsteroidsTraficBetweenDate(){
        const params = {
            sdate:Constants.START_DATE_KEY + "=" + encodeURIComponent(this.state.start_date), 
            edate:Constants.END_DATE_KEY + "=" + encodeURIComponent(this.state.end_date),
            detailed:Constants.DETAILED_KEY + "=" + encodeURIComponent(false),
            api_key:Constants.API_KEY + "=" + encodeURIComponent(Constants.API_KEY_VALUE)
        };

        fetch(Constants.BASE_URL + `/feed?${params.sdate}&${params.edate}&${params.detailed}&${params.api_key}`) 
            .then(res => res.json())
            .then(
                (result) => {
                    let values;
                    if(Constants.ELEMENT_COUNT_KEY in result){
                        values = {
                            start_date: this.state.start_date,
                            end_date: this.state.end_date,
                            asteroids: result[Constants.NEAR_EARTH_OBJECTS_KEY],
                            isLoaded: true
                        }
                    }
                    if(Constants.CODE_KEY in result){
                        values = {
                            isError: true,
                            code:result[Constants.CODE_KEY],
                            error_message:result[Constants.ERROR_MESSAGE_KEY],
                        }
                    }
                    this.props.onSubmit(values)
                }
            )
    }

    render() {
        const {start_date, end_date} = this.state
        return (
            <form className="form" onSubmit={this.handleSubmit}>
                <label>Start date : 
                <input 
                    type="date" 
                    name="start_date" 
                    value={start_date} 
                    onChange={this.handleChange}
                    />
                </label>
                <label>End date : 
                <input 
                    type="date" 
                    name="end_date" 
                    value={end_date} 
                    onChange={this.handleChange}
                    />
                </label>
            <button className="form-button">Envoyer</button>
            </form>
        );
    }
  } 
   
export default Form
   
   
   
   
  