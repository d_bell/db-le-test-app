// Api values
export const BASE_URL = "https://api.nasa.gov/neo/rest/v1"
export const API_KEY_VALUE = "VXeO05eWTYQR8cHfxSY24fxb7JMR0CztuPrtBUtV"
export const START_DATE_KEY = "start_date"
export const END_DATE_KEY = "end_date"
export const DETAILED_KEY = "detailed"
export const API_KEY = "api_key"


// Api result keys
export const NEAR_EARTH_OBJECTS_KEY = "near_earth_objects"
export const ELEMENT_COUNT_KEY = "element_count"
export const CODE_KEY = "code"
export const ERROR_MESSAGE_KEY = "error_message"

export const NEO_REFERENCE_ID_KEY = "neo_reference_id"
export const NAME_KEY = "name"
export const ESTIMATED_DIAMETER_KEY = "estimated_diameter"
export const KILOMETERS_KEY = "kilometers"
export const ESTIMATED_DIAMETER_MIN_KEY = "estimated_diameter_min"
export const ESTIMATED_DIAMETER_MAX_KEY = "estimated_diameter_max"
export const CLOSE_APPROACH_DATA_KEY = "close_approach_data"
export const CLOSE_APPROACH_DATE_KEY = "close_approach_date"
export const MISS_DISTANCE_KEY = "miss_distance"