import './App.css';
import AsteroidList from "./asteroid/AsteroidList";
import { Component } from 'react';
import Form from './form/Form';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      startDate: '',
      endDate: '',
      asteroids: [],
      isError: false,
      isLoading: true,
      isSearching: false,
      isLoaded: false,
    };
  }

  getResponse = (response) => {
    if(response["isSearching"]){
      this.setState({
        isError: false,
        errorMessage: "",
        isLoading: false,
        isSearching: true,
        isLoaded: false,
      });
    }

    if(response["isLoaded"]){
      this.setState({
        startDate: response.start_date,
        endDate: response.end_date,
        asteroids: response.asteroids,
        isError: false,
        isLoading: false,
        isSearching: false,
        isLoaded: true
      });
    }

    if(response["isError"]){
      this.setState({
        isError: true,
        isLoading: false,
        isSearching: false,
        isLoaded: false,
        errorMessage: "Code " + response["code"] + ":" + response["error_message"]
      });
    }
  }

  render(){

    const {isLoading, isSearching, isLoaded, isError} = this.state

    let asteroidListView;

    // Manage api request error
    if(isError){
      asteroidListView = (
        <AsteroidList 
            isLoading = {this.state.isLoading}
            isSearching = {this.state.isSearching}
            isLoaded = {this.state.isLoaded}
            isError = {this.state.isError}
            message = {this.state.errorMessage}
        ></AsteroidList>
      )
    }

    // Manage first page loading
    if(isLoading){
      asteroidListView = (
        <AsteroidList 
            isLoading = {this.state.isLoading}
            isSearching = {this.state.isSearching}
            isLoaded = {this.state.isLoaded}
            isError = {this.state.isError}
            message = "Select start and end date"
        ></AsteroidList>
      )
    }

    // Manage waiting when user request asteroids
    if(isSearching){
      asteroidListView = (
        <AsteroidList 
            isLoading = {this.state.isLoading}
            isSearching = {this.state.isSearching}
            isLoaded = {this.state.isLoaded}
            isError = {this.state.isError}
            message = "Searching asteroids..."
        ></AsteroidList>
      )
    }

    // Manage asteroids result
    if(isLoaded){
      asteroidListView =  (
        <AsteroidList 
              asteroids = {this.state.asteroids}
              start_date = {this.state.startDate}
              end_date = {this.state.endDate}
              isLoaded = {this.state.isLoaded}
        ></AsteroidList>
      )
    }

    return (
      <div className>
        <Form onSubmit={this.getResponse}></Form>
        <section className="content">
          {asteroidListView}
        </section>
    </div>
    );
  }
}

export default App;
