import '../App.css';
import React, {Component} from 'react';
import AsteroidDetailCard from './AsteroidDetailCard';
import * as Constants from "../constants/constants"

class AsteroidDetailList extends Component{
    render(){
        const props = this.props;

        let asteroidDetailsView; 

        const approchData = Object.keys(props.closeApproachData)

        if(approchData.length === 0) {
            return <h3> Select item to get details from asteroid</h3>;
        }

        if(approchData.length  > 0){
            const fiveElements = props.closeApproachData.close_approach_data.slice(0, 5);
            asteroidDetailsView = fiveElements.map(data => {
                return(
                    <AsteroidDetailCard
                        date = {data[Constants.CLOSE_APPROACH_DATE_KEY]}
                        distance = {data[Constants.MISS_DISTANCE_KEY][Constants.KILOMETERS_KEY]}
                    ></AsteroidDetailCard>
                )
            })
        }

        return(
            <section className="asteroid-detail-list">
                {asteroidDetailsView}
            </section>
        )
    }
}

export default AsteroidDetailList