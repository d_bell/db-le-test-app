import React, {Component} from 'react';
import '../App.css';

class AsteroidDetailCard extends Component{
    render(){
        const props = this.props;
        return(
            <section className="asteroid-detail-card">
                <label><b>Passing date :</b> {props.date}</label>
                <label><b>Passing distance :</b> {props.distance}</label>  
            </section>
        )
    }
}

export default AsteroidDetailCard